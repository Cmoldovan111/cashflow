sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("Cashflow.controller.View1", {

		onInit: function(oEvent) {
			console.log("Controller Initializat");
			// var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
			// 	pattern: "YYYYMMdd"
			// });
			// var date = dateFormat.format(new Date());
			// console.log(date);
		},

		onAfterRendering: function() {
			this.getView().getModel("TSPIRQ").setUseBatch(false);
		},

		onPressEdit: function() {
			var that = this;
			// this.getView().byId("__table0").setMode("None");
			// var items = this.getView().byId("__table0").getItems();
			// items.forEach(function(item) {
			// 	item.setType("Detail");
			// });
			// this.getView().byId("datepicker").setEditable(true);
			// this.getView().byId("input1").setEditable(true);
			// this.getView().byId("input2").setEditable(true);
			console.log(sap.ui.core.Fragment.byId("DialogId", "edit1"));
			console.log(this.getView().byId(this.getView().createId("edit1")));
			console.log(that.getView().byId("edit2"));

		},

		// onPressDelete: function() {
		// 	var items = this.getView().byId("__table0").getItems();
		// 	items.forEach(function(item) {
		// 		item.setType("Inactive");
		// 	});
		// 	this.getView().byId("__table0").setMode("Delete");
		// },

		// onPressDetail: function() {
		// 	this.getView().byId("__table0").setMode("None");
		// 	var items = this.getView().byId("__table0").getItems();
		// 	items.forEach(function(item) {
		// 		item.setType("Active");
		// 	});
		// },
		onPressAdd: function(){
			var that = this;
			if(!this.dialogAdd){
			this.dialogAdd = sap.ui.xmlfragment("Cashflow.view.Add", that);
			}
			this.dialogAdd.open();
		},
		onPressAddBack: function() {
			var that = this;
			that.dialogAdd.close();
			console.log(this.dialogAdd);
			console.log(this.dialogDetail);
		},
		onPressEditBack: function() {
			var that = this;
			that.dialogDetail.close();
			console.log(this.dialogAdd);
			console.log(this.dialogDetail);
		},
		onSelectionChange: function(oEvent) {
			console.log("HERE");
			var that = this;

			var item = new sap.ui.model.json.JSONModel(oEvent.getSource().getBindingContext("TSPIRQ").getProperty());
			//var oItem2 = oEvent.getSource().getBindingContext("TSPIRQ").getProperty(oEvent.getSource().getBindingContext("TSPIRQ").getPath());
			if(!this.dialogDetail){
			this.dialogDetail = sap.ui.xmlfragment("Cashflow.view.Detail", that);
			}
			this.dialogDetail.open();
			console.log(this.dialogDetail);
			this.dialogDetail.setModel(item, "item");
		},
		
		// editIcon: function(oEvent) {
		// 	console.log("editIcon");
		// 	var that = this;
		// 	var item = new sap.ui.model.json.JSONModel(oEvent.getSource().getBindingContext("TSPIRQ").getProperty());
		// 	//var oItem2 = oEvent.getSource().getBindingContext("TSPIRQ").getProperty(oEvent.getSource().getBindingContext("TSPIRQ").getPath());
		// 	this.dialog = sap.ui.xmlfragment("Cashflow.view.Edit", that);
		// 	this.dialog.open();
		// 	this.dialog.setModel(item, "item");
		// },

		// onCalendarSelect: function(oEvent) {
		// 	var that = this;
		// 	var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
		// 		pattern: "YYYYMMdd"
		// 	});
		// 	var startDate = dateFormat.format(new Date(oEvent.getSource().getDateValue()));
		// 	var endDate = dateFormat.format(new Date());
		// 	console.log(startDate);
		// 	console.log(endDate);
		// 	var ddd = this.getView().byId("__table0").getBinding("items").filter(new sap.ui.model.Filter("A0CALDAY", "BT", startDate, endDate));
		// 	console.log(ddd);

		// },
		// 		onCalendarSelect2: function(oEvent) {
		// 	var that = this;
		// 	var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
		// 		pattern: "YYYYMMdd"
		// 	});
		// 	var startDate = dateFormat.format(new Date(oEvent.getSource().getSelectedDates()[0].getStartDate()));
		// 	var endDate = dateFormat.format(new Date(oEvent.getSource().getSelectedDates()[0].getEndDate()));
		// 	console.log(startDate);
		// 	console.log(endDate);
		// 	var ddd = this.getView().byId("__table0").getBinding("items").filter(new sap.ui.model.Filter("A0CALDAY", "BT", startDate, endDate));
		// 	console.log(ddd);

		// },

		// switchChart: function(){
		// 	var that = this;
		// 	this.dialog =  sap.ui.xmlfragment("Cashflow.view.Chart", that);
		// 	this.dialog.open();
		// },

	});
});